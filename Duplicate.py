from lib import (Parkour,
                 Tracker,
                 ParentRegister,
                 NodeIdRegister,
                 ScopeRegister,
                 Thesis)


class TrackerRegister(Tracker):
    def __init__(self, *args, **kwargs):
        super(TrackerRegister, self).__init__(self.fortrack, self.backtrack, *args, **kwargs)
        self.inFunc = False

    def fortrack(self, motherofall, node, parent, *args):
        if type(node).__name__ == "BlockStmt"\
                and type(parent).__name__ == "Decl":
            self.inFunc = True
        if self.inFunc is False:
            return
        skip = [
            "list",
            "str",
            "int",
            "NoneType",
        ]
        name = type(node).__name__
        if name in skip:
            return
        if len(motherofall.getChildren(node)) != 0:  # not a terminal node
            return
        if name not in motherofall.endings:
            motherofall.endings[name] = []
        motherofall.endings[name].append(node)
        return

    def backtrack(self, motherofall, node, parent, *args):
        if type(node).__name__ == "BlockStmt"\
                and type(parent).__name__ == "Decl":
            self.inFunc = False
        pass


class Duplicate(Parkour):

    def __init__(self, filename, mindepth, verbosity, report_file):
        super(Duplicate, self).__init__(filename, verbosity)
        self.nodeid = 0
        self.symid = 0
        self.mindepth = mindepth
        self._report_file = report_file
        self._thesis = Thesis(self._report_file)
        self.endings = {}
        self.attrs = [
            "body",
            "call_expr",
            "condition",
            "ctype",
            "elsecond",
            "expr",
            "increment",
            "init",
            "params",
            "thencond",
        ]
        self.duplicates = []

    def run(self, *args, **kwargs):
        super(Duplicate, self).run(*args, **kwargs)
        self.compareEndings()
        self.render()

    def getPrintableNode(self, node):
        printable = [
            "Decl",
            "RootBlockStmt"
        ]
        if not type(node).__name__ in printable:
            return self.getPrintableNode(node.ADBG_parent())
        return node

    def printNode(self, node, fromRoot=False):
        if fromRoot is True:
            return self.getPrintableNode(node).to_c()
        else:
            return node.to_c()

    def render(self):
        self._thesis.title(str(len(self.duplicates))+" duplicates")
        for dup in self.duplicates:
            n1 = dup[0]
            n2 = dup[1]
            depth = dup[2]
            self._thesis.title("Possible duplicate (depth = "
                               + str(depth) + "):", 2)
            self._thesis.title("In:", 3)
            self._thesis.code(self.printNode(n1, True))
            self._thesis.title("And:", 3)
            self._thesis.code(self.printNode(n2, True))
            self._thesis.title("Similarities:", 3)
            self._thesis.code(self.printNode(n1))
            self.printNode(n1)
            self._thesis.code(self.printNode(n2))

    def duplicateExists(self, n1, n2):
        for dup in self.duplicates:
            if dup[0].ADBG_node_id == n1.ADBG_node_id\
                    and dup[1].ADBG_node_id == n2.ADBG_node_id:
                return True
        return False

    def compareEndings(self):
        for k in self.endings:
            ending = self.endings[k]
            for i in range(0, len(ending)):
                n1 = ending[i]
                for j in range(i+1, len(ending)):
                    n2 = ending[j]
                    depth = self.compareNodes(n1, n2, idTables=[{}, {}])
                    if depth < self.mindepth:
                        continue
                    rn1 = self.getParent(n1, depth)
                    rn2 = self.getParent(n2, depth)
                    if self.duplicateExists(rn1, rn2):
                        continue
                    self.duplicates.append([rn1, rn2, depth])

    def getParent(self, node, depth):
        if depth == 0:
            return node
        if node.ADBG_parent() is None:
            return node
        return self.getParent(node.ADBG_parent(), depth - 1)

    def compareChildren(self, n1, n2, idTables, skip):
        n1_kids = self.getChildren(n1)
        n2_kids = self.getChildren(n2)
        for i in range(0, len(n1_kids)):
            kid1 = n1_kids[i]
            kid2 = n2_kids[i]
            if skip is not None\
                    and kid1.ADBG_node_id == skip[0]\
                    and kid2.ADBG_node_id == skip[1]:
                continue
            depth = self.compareNodes(kid1, kid2, idTables, crawlUp=False)
            if depth == -1:
                return False
        return True

    def compareParents(self, n1, n2, idTables):
        return 1 + self.compareNodes(
            n1.ADBG_parent(),
            n2.ADBG_parent(),
            idTables,
            skip=[n1.ADBG_node_id, n2.ADBG_node_id]
        )

    def isMerging(self, n1, n2):
        return n1.ADBG_node_id == n2.ADBG_node_id

    def compareEssence(self, n1, n2):
        if n1 is None or n2 is None:
            return False
        if self.isMerging(n1, n2):  # trees are merging
            return False
        if type(n1).__name__ != type(n2).__name__:
            return False
        if type(n1).__name__ in ["Raw", "Literal"]:  # + - * / ++ 42 "toto" ...
            if n1.value != n2.value:
                return False
        if not self.compareAttrs(n1, n2):
            return False
        return True

    def idIsFunc(self, node):
        return type(node.ADBG_parent()).__name__ == "Func"

    def getId(self, node, idTable, default=None):
        if node.value in idTable.keys():
            return idTable[node.value]
        if default is not None:
            idTable[node.value] = default
        else:
            idTable[node.value] = self.symid
            self.symid += 1
        return idTable[node.value]

    def compareFuncs(self, n1, n2, idTables):
        id1 = self.getId(n1, idTables[0])
        id2 = self.getId(n2, idTables[1], id1)
        if id1 != id2:
            return False
        s1 = self.getScope(n1)
        s2 = self.getScope(n2)
        if s1 is None or s2 is None:
            return n1.value == n2.value
        if s1["symbol"]["id"] == s2["symbol"]["id"]:
            return True
        if s1["scope"]["type"] != s2["scope"]["type"]:
            return False
        ret =  self.compareNodes(s1["symbol"]["ctype"], s2["symbol"]["ctype"],
                                 idTables, crawlUp=False)
        return ret == 0

    def compareIds(self, n1, n2, idTables):
        if self.idIsFunc(n1) and self.idIsFunc(n2):
            return self.compareFuncs(n1, n2, idTables)
        id1 = self.getId(n1, idTables[0])
        id2 = self.getId(n2, idTables[1], id1)
        if id1 != id2:
            return False
        s1 = self.getScope(n1)
        s2 = self.getScope(n2)
        if s1 is None or s2 is None:  # TODO: what should be the default ?
            return False
        if s1["symbol"]["id"] == s2["symbol"]["id"]:
            return True
        if s1["scope"]["type"] != s2["scope"]["type"]:
            return False
        ret =  self.compareNodes(s1["symbol"]["ctype"], s2["symbol"]["ctype"],
                                 idTables, crawlUp=False)
        return ret == 0

    def compareAttrs(self, n1, n2):
        exclude = [
            "value",
            "ADBG_node_id",
            "_name",
        ]
        for k in n1.__dict__.keys():
            if k in exclude:
                continue
            v1 = n1.__dict__[k]
            if type(v1).__name__ not in ["str", "int"]:
                continue
            if k not in n2.__dict__.keys():
                return False
            v2 = n2.__dict__[k]
            if type(v1).__name__ != type(v2).__name__:
                return False
            if v1 != v2:
                return False
        return True

    def isFunction(self, node):
        if type(node).__name__ != "Decl":
            return False
        if type(node._ctype).__name__ != "FuncType":
            return False
        return True

    def compareNodes(self, n1, n2, idTables, skip=None, crawlUp=True):
        if not self.compareEssence(n1, n2):
            return -1
        if self.isFunction(n1) or self.isFunction(n2):
            return -1
        if type(n1).__name__ == "Id":
            if not self.compareIds(n1, n2, idTables):
                return -1
        if len(self.getChildren(n1)) != len(self.getChildren(n2)):
            return -1
        if not self.compareChildren(n1, n2, idTables, skip):
            return -1
        if crawlUp is False:
            return 0
        return self.compareParents(n1, n2, idTables)

    def checkAttr(self, attr, node1, node2):
        return hasattr(node1, attr) == hasattr(node2, attr)

    @ScopeRegister()
    @ParentRegister()
    @NodeIdRegister()
    @TrackerRegister()
    def switch_func(self, node, parent, *args, **kwargs):
        return super(Duplicate, self).switch_func(node, parent,
                                                  *args, **kwargs)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import (ArgumentParser, FileType)
import logging

try:
    from lib import (Logger, LoggingAction)
    from AutomaticDebugger import (Duplicate)
except:
    import sys
    sys.path.append('../../')

    from lib import (Logger, LoggingAction)
    from AutomaticDebugger.Duplicate import (Duplicate)


def command_line(cl_parser=ArgumentParser(prog='Duplicate')):
    file_help_message = """
        We expectes c files
    """
    verbosity_help_message = """
    There is five levels of verbosity : DEBUG, INFO, WARNING, ERROR, CRITICAL
    By default the verbosity is set in INFO level
    """
    report_file_help = """
        create a report file and write in it
    """
    report_append_help = """
        Append report file
    """
    cl_parser.add_argument("files", nargs='+', type=FileType('r'),
                           help=file_help_message)
    cl_parser.add_argument("--report", type=FileType('w'), nargs='?',
                           dest="report_file", help=report_file_help)
    cl_parser.add_argument("--report-append", type=FileType('a'), nargs='?',
                           dest="report_file", help=report_append_help)
    cl_parser.add_argument("-v", "--verbose", default=logging.INFO, type=str,
                           dest="verbosity", action=LoggingAction,
                           help=verbosity_help_message)
    cl_parser.set_defaults(run=run)
    return cl_parser


def run(args):
    logger = Logger.getLogger(args.verbosity, "Duplicate")
    for c_file in args.files:
        logger.info("Begining the parkour of {file}".format(file=c_file.name))
        p = Duplicate.Duplicate(c_file.name,
                                args.min_depth,
                                args.verbosity,
                                args.report_file)
        p.run()
        logger.info("Ending the parkour of {file}".format(file=c_file.name))


def get_sub_parser(parser):
    Duplicate_help_message = """
        Duplicate module
    """
    cl_parser = parser.add_parser("Duplicate", help=Duplicate_help_message)
    cl_parser.add_argument(
        "--min_depth", metavar="m", type=int, default=3,
        help="minimum depth to concider a duplicate (default = 3)")
    return command_line(cl_parser)


def main():
    args = command_line().parse_args()
    logger = Logger.getLogger(args.verbosity, "Duplicate")
    for c_file in args.files:
        logger.info("Begining the parkour of {file}".format(file=c_file.name))
        p = Duplicate(c_file.name,
                      args.min_depth,
                      args.verbosity,
                      args.report_file)
        p.run()
        logger.info("Ending the parkour of {file}".format(file=c_file.name))

if __name__ == '__main__':
    main()
